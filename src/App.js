import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { InitTRTC, InitVOD, InitUpload } from './components'

function App() {
  return (
    <div className="App">
      <InitTRTC />

      <InitUpload />

      <InitVOD />

      
    </div>
  );
}

export default App;
