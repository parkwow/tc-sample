import React, { useState, useEffect } from 'react';
import { Button } from 'antd';


const InitVOD = props => {

  const [ tcplayer, setTcplayer ] = useState();

    useEffect(() => {
      if(window.tcplayer) {
        const TCPlayer = window.tcplayer;
        const _tcplayer = TCPlayer("player-container-id", { // player-container-id is the player container ID, which must be the same as in html
          fileID: "5285890804808129717", // Enter the fileID of the video to be played (required).
          appID: "1302397712", // Enter the appID of the VOD account (required).
          autoplay: true // Whether to enable auto playback,
        });

        setTcplayer(_tcplayer);
      }      
    }, []);

    useEffect(() => {
      console.log('tcplayer : ', tcplayer);
      window.onload = function(){
        changeValue();
      }
  
    }, [tcplayer]);
    
    const getCurrTime = () => {
      console.log('tcplayer : ', tcplayer.cache_.currentTime);
    }

    const changeValue = () => {
      
      let menu = document.getElementsByClassName('.tcp-video-quality-switcher');
      console.log(menu)
      /*for (const i in menu) {
       console.log(i) 
      }*/
      //console.log('menu ', menu);
    }

    return (
        <div>
          
          <video id="player-container-id" preload="auto" width="852" height="480" playsinline webkit-playsinline x5-playsinline></video>
          <Button onClick={getCurrTime} style={{width: 200, height: 100 }}>getCurrTime</Button>
        </div>
    );
}

export default InitVOD
