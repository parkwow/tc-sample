import axios from 'axios';
import { getTrackingId } from 'lib/utils';

const getAPIVersion = () => {
  return 'v1/';
}

const extractErrorMsg = (error) => {
  if (!error.status && !error.response.status) {
    return '서버에 접속할 수 없습니다';
  } else {
    return error.response.data.error.message || '에러 발생';
  }
}

const getHost = () => {
  return 'http://localhost:10151/api/' + getAPIVersion();
};

const host = getHost();

// api protocols
export const get = (path, data, callback, token) => {
  let options = data ? {
    params: {
      ...data
    }
  } : { params: {}};
  options.headers = {
    'Accept': 'application/json',
  };

  if (token) {
    options.headers['X-Access-Token'] = getTrackingId();
  }

  return axios.get(host + path, options)
    .then(response => {
      if (callback) {
        return callback(response);
      }
    })
    .catch(err => {
      throw extractErrorMsg(err);
    });
};

export const post = (path, data, callback, withCredentials) => {
  return axios.post(host  + path, data ? data : {}, {
    headers: {
      'Accept': 'application/json',
    },
    withCredentials: !!withCredentials,
  })
    .then(response => {
      if (callback) {
        return callback(response);
      }
    })
    .catch(err => {
      if (err.response && err.response.data.error.message === 'NOT PAID') {
        alert('정상적인 결제가 아닙니다');
        window.location.href = '/';
      } else {
        throw extractErrorMsg(err);
      }
    });
};

export const patch = (path, data, callback) => {
  return axios.patch(host + path, data ? data : {}, {
    headers: {
      'Accept': 'application/json',
    },
  })
    .then(response => {
      if (callback) {
        return callback(response);
      }
    })
    .catch(err => {
      throw extractErrorMsg(err);
    });
};

export const put = (path, data, callback, withCredentials) => {
  return axios.put(host  + path, data ? data : {}, {
    headers: {
      'Accept': 'application/json',
    },
    withCredentials: !!withCredentials,
  })
    .then(response => {
      if (callback) {
        return callback(response);
      }
    })
    .catch(err => {
      if (err.response && err.response.data.error.message === 'NOT PAID') {
        alert('정상적인 결제가 아닙니다');
        window.location.href = '/';
      } else {
        throw extractErrorMsg(err);
      }
    });
};


export const remove = (path, data, callback, withCredentials) => {
  return axios.delete(host  + path, {data: data ? data : {}}, {
    headers: {
      'Accept': 'application/json',
    },
    withCredentials: !!withCredentials,
  })
    .then(response => {
      if (callback) {
        return callback(response);
      }
    })
    .catch(err => {
      if (err.response) {
        alert('처리중 오류가 발생했습니다');
        window.location.href = '/';
      } else {
        throw extractErrorMsg(err);
      }
    });
};


export const postByMultipart = (path, data, callback, withCredentials) => {
  const formData = new FormData();

  for (let key in data.params) {
    formData.append(key, data.params[key]);
  }

  return axios
    .post(host + path, formData, {
      headers: {
        Accept: 'application/json'
      },
      withCredentials: !!withCredentials,
    })
    .then(response => {
      if (callback) {
        return callback(response)
      }
    })
    .catch(err => {
      throw extractErrorMsg(err)
    })
};