import React, { useState } from 'react';
import TcVod from 'vod-js-sdk-v6';
import { Button, Upload, message } from 'antd';

const InitUpload = props => {
  const [loading, setLoading] = useState(false);

  function getSignature() {
    //signature api 로 가져오면 됨
    return '10YhBfoRFNR4ryZ/ZxbyXBPIP+RzZWNyZXRJZD1BS0lESml4MG5mQWx4NTFqTWtzT2NodHBmSGRoWFVCZzY3NTImY3VycmVudFRpbWVTdGFtcD0xNTk0MTc2NjE5JmV4cGlyZVRpbWU9MTU5NDI2MzAxOSZyYW5kb209MTAzNjg5Nzg5MQ==';
  };

  const handleBeforeUpload = file => {
    //파일 업로드 핸들링 하기 전
    console.log('handleBeforeUpload file : ', file);
  }
  
  const handleUpload = file => {
    //파일 업로드 핸들링
    console.log('handleUpload file : ', file);
    if (file.file.status === 'uploading') {
      setLoading(true);
    } else if (file.file.status === 'done') {
      setLoading(false);
      uploadLetsgo(file.file.originFileObj);
    } else if (file.file.status === 'error') {
      message.error(`${file.file.name} 파일 업로드가 실패하였습니다.`);
    } else if (file.file.status === 'removed') {
      
    }
  };

  const uploadLetsgo = (file) => {
    console.log('file : ', file);
    // If imported through import, run new TcVod(opts)
  // new TcVod.default(opts) is imported through script
  console.log('TcVod: ', TcVod);
  const tcVod = new TcVod({
    getSignature: getSignature // The function to get the upload signature described above
  });

  const uploader = tcVod.upload({
    mediaFile: file, // Media file (video, audio, or image) in File type
  })
  uploader.on('media_progress', function(info) {
    console.log(info.percent) // Progress
  })

  // Callback result description
  // type doneResult = {
  //   fileId: string,
  //   video: {
  //     url: string
  //   },
  //   cover: {
  //     url: string
  //   }
  // }
  uploader.done().then(function (doneResult) {
    console.log('doneResult : ', doneResult);
    // Deal with doneResult
  }).catch(function (err) {
    // Deal with error
    console.log('err', err);
  })
  }


  return (

    <Upload
        onChange={file => handleUpload(file)}
        beforeUpload={file => handleBeforeUpload(file)}
        customRequest={({onSuccess, file}) => {
          setTimeout(() => {
            onSuccess('ok');
          }, 0);
        }}
        accept="image/png, image/jpeg, video/mp4"
      >
        <Button
          icon="upload"
        >
          업로드
        </Button>
      </Upload>
  );

}

export default InitUpload