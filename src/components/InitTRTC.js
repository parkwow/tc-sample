import React, { useState, useEffect } from 'react';
import TRTC from 'trtc-js-sdk';
import TLSSigAPIv2 from 'tls-sig-api-v2';
import { Button } from 'antd';

const InitTRTC = () => {

  const [client, setClient] = useState();
  const [localstream, setLocalStream] = useState();
  

  useEffect(() => {
    console.log('useEffect');
    if (!client) {
      console.log('client');
      const _client = TRTC.createClient({ mode: 'live', sdkAppId: '1400392544', userId: 'JSTest', userSig: getSig() });
      setClient(_client)
    }

    if (!localstream) {
      console.log('localstream');
      const _localStream = TRTC.createStream({ audio: true, video: true });
      setLocalStream(_localStream);
      // navigator.mediaDevices.getUserMedia({ audio: true, video: true }).then(stream => {
      //   const audioTrack = stream.getAudioTracks()[0];
      //   const videoTrack = stream.getVideoTracks()[0];
      
      //   // 对audioTrack和videoTrack处理之后，
      
      //   const _localStream = TRTC.createStream({ audioSource: audioTrack, videoSource: videoTrack });
      //   setLocalStream(_localStream);
      // });
    }
  });

  const check = () => {
    TRTC.checkSystemRequirements()
    .then(res => {
      alert(`Your browser is compatible with TRTC : ${res}`);
    })
    .catch(error => {
      alert(error)
    });
  };

  const isScreenShare = () => {
    alert(`Your browser is isScreenShareSupported : ${TRTC.isScreenShareSupported()}`);
  };

  const getDevices = () => {
    TRTC.getDevices()
    .then(res => {
      alert(`Your supported devices is : ${res}`);
      console.log('getDevices : ', res);
    })
    .catch(error => {
      alert(error)
    });
  };

  const getCameras = () => {
    TRTC.getCameras()
    .then(res => {
      alert(`Your supported devices is : ${res}`);
      console.log('getDevices : ', res);
    })
    .catch(error => {
      alert(error)
    });
  };

  const getMicrophones = () => {
    TRTC.getMicrophones()
    .then(res => {
      alert(`Your supported devices is : ${res}`);
      console.log('getDevices : ', res);
    })
    .catch(error => {
      alert(error)
    });
  };

  const getSpeakers = () => {
    TRTC.getSpeakers()
    .then(res => {
      alert(`Your supported devices is : ${res}`);
      console.log('getDevices : ', res);
    })
    .catch(error => {
      alert(error)
    });
  };

  const getSig = () => {
    const api = new TLSSigAPIv2.Api(1400392544, "a0fb76c217e7d2efbcd175be05ff7943113a9160a13413f1cb3923d1ba32277c");
    const sig = api.genSig("JSTest", 86400*180);
    console.log(sig);
    return sig;
  }

  const joinRoom = () => {
    if (client) {
      client.join({ roomId: 8889, role: 'anchor' }).then((res) => {
        console.log('join room success', res);
      }).catch(error => {
        console.error('Join room failed: ' + error);
      });
    }
  }

  const leaveRoom = () => {
    if (client) {
      client.leave().then((res) => {
        console.log('leaving room success', res);
      }).catch(error => {
        console.error('leaving room failed: ' + error);
      });
    }
  }

  const publish = () => {
    if (client && localstream) {
      localstream.initialize().catch(error => {
        console.error('failed initialize localStream ' + error);
      }).then(() => {
        console.log('initialize localStream success');
        // 本地流初始化成功，可通过Client.publish(localStream)发布本地音视频流
        client.publish(localstream).then(() => {
          // 本地流发布成功
          console.log('publish ~~!!');
        });
      });
    }
  }

  const unPublish = () => {
    if (client && localstream) {
      client.unpublish(localstream).then(() => {
        // 取消发布本地流成功
      });
    }
  }


  const upload = () => {
    
  } 
  

  return (
    <div> 
        <p>InitTRTC</p>
        <script src="trtc.js"></script>
        <div>
          <Button onClick={check} style={{width: 200, height:50}}>
            checkSystem
          </Button>
          <Button onClick={isScreenShare} style={{width: 200, height:50}}>
            isScreenShare
          </Button>
          <Button onClick={getDevices} style={{width: 200, height:50}}>
            getDevices
          </Button>
          <Button onClick={getCameras} style={{width: 200, height:50}}>
            getCameras
          </Button>
          <Button onClick={getMicrophones} style={{width: 200, height:50}}>
            getMicrophones
          </Button>
          <Button onClick={getSpeakers} style={{width: 200, height:50}}>
            getSpeakers
          </Button>
        </div>
        <div>
          <Button onClick={getSig} style={{width: 200, height:50}}>
            getSig
          </Button>
          <Button onClick={joinRoom} style={{width: 200, height:50}}>
            joinRoom
          </Button>
          <Button onClick={leaveRoom} style={{width: 200, height:50}}>
            leaveRoom
          </Button>
          <Button onClick={publish} style={{width: 200, height:50}}>
            publish
          </Button>
          <Button onClick={unPublish} style={{width: 200, height:50}}>
            unPublish
          </Button>
        </div>
        
    </div>
      
  )
}


export default InitTRTC